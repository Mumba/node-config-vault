/**
 * VaultLoader tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from "assert";
import {GenericSecret} from "mumba-vault";
import {VaultLoader} from "../../src/VaultLoader";

describe('VaultLoader unit tests', () => {
	var instance: VaultLoader;
	var vaultStub: GenericSecret;

	beforeEach(() => {
		vaultStub = Object.create(VaultLoader);
		instance = new VaultLoader(vaultStub);
	});

	it('should throw if vault is missing as a constructor argument', () => {
		assert.throws(() => {
			new VaultLoader(void 0);
		}, /<vault> required/);
	});

	it('should have the correct name', () => {
		assert.equal(instance.getName(), 'vault');
	});

	it('should load from vault', () => {
		vaultStub.get = (token: string, path: string) => {
			return Promise.resolve({
				data: { foo: 'bar' }
			});
		};

		return instance.load({
				token: 'the-token',
				path: 'the-path'
			})
			.then((result: any) => {
				assert.equal(result.foo, 'bar');
			})
	});
});
