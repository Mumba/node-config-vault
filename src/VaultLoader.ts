/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Loader} from "mumba-config";
import {GenericSecret} from "mumba-vault";

export interface VaultLoaderOptions {
	token: string;
	path: string;
}

/**
 * Vault loader for Config.
 */
export class VaultLoader implements Loader {
	private vault: GenericSecret;

	/**
	 * Constructor
	 *
	 * @param {GenericSecret} vault
	 */
	constructor(vault: GenericSecret) {
		if (!vault) {
			throw new Error('VaultLoader: <vault> required.');
		}

		this.vault = vault;
	}

	/**
	 * Get the name of the loader.
	 *
	 * @returns {string}
	 */
	public getName(): string {
		return 'vault';
	}

	/**
	 * Load data from Vault.
	 * 
	 * @param {VaultLoaderOptions} options
	 * @returns {Promise<object>}
	 */
	public load(options: VaultLoaderOptions): Promise<any> {
		return this.vault.get(options.token, options.path)
			.then((res: any) => {
				return res.data;
			});
	}
}
