# Mumba Vault

A Vault configuration loader for [Mumba Config](https://gitlab.com/Mumba/node-config).

## Installation 

```sh
$ npm install mumba-config-vault
```

## Example

### TypeScript

```typescript
import {Config} from "mumba-config";
import {GenericSecret} from "mumba-vault";
import {VaultLoader} from "mumba-config-vault";

let config = new Config();
let vault = new GenericSecret();
let vaultLoader = new VaultLoader(vault);

config.registerLoader(vaultLoader);

config.load([
		{
			name: 'vault',
			token: '442eec1e-ba12-e000-91dc-78fafbdb8015',
			path: '/v1/secret/the/path'
		}
	])
	.then(() => {
	  // Vault secrets are now loaded into the config object.
	})
	.catch(console.error);
```

## Code quality and tests

```sh
$ npm run test
```

## License

Apache-2.0

## References

* https://www.vaultproject.io
* https://github.com/Microsoft/TypeScript/wiki
* http://mochajs.org

* * *

&copy; 2016 Mumba Pty Ltd. All rights reserved.



